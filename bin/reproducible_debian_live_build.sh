#!/bin/bash
# vim: set noexpandtab:

# Copyright 2021-2022 Holger Levsen <holger@layer-acht.org>
# Copyright 2021-2022 Roland Clobus <rclobus@rclobus.nl>
# released under the GPLv2

# Coding convention: enforced by 'shfmt'

DEBUG=false
. /srv/jenkins/bin/common-functions.sh
common_init "$@"

# common code for tests.reproducible-builds.org
. /srv/jenkins/bin/reproducible_common.sh
set -e
set -o pipefail # see eg http://petereisentraut.blogspot.com/2010/11/pipefail.html

output_echo() {
	set +x
	echo "###########################################################################################"
	echo
	echo -e "$(date -u) - $1"
	echo
	if $DEBUG; then
		set -x
	fi
}

cleanup() {
	output_echo "Publishing results."

	if [ "$1" == "success" ]; then
		# Publish the ISO image and its summary
		export PAGE=last-reproducible-image.txt
		cp -a ${RESULTSDIR}/summary_build1.txt ${PAGE}
		publish_page debian_live_build

		export PAGE=last-reproducible-image.iso
		cp -a ${RESULTSDIR}/b1/${PROJECTNAME}/${CONFIGURATION}/live-image-amd64.hybrid.iso ${PAGE}
		publish_page debian_live_build

		export PAGE=${CONFIGURATION}-${DEBIAN_VERSION}.txt
		mv ${RESULTSDIR}/summary_build1.txt ${PAGE}
		rm ${RESULTSDIR}/summary_build2.txt
		output_echo "Info: no differences found."
	else
		if [ -f "${RESULTSDIR}/${PROJECTNAME}/${CONFIGURATION}/live-image-amd64.hybrid.iso.html" ]; then
			# Publish the output of diffoscope, there are differences
			export PAGE=${CONFIGURATION}-${DEBIAN_VERSION}.html
			cp -a ${RESULTSDIR}/${PROJECTNAME}/${CONFIGURATION}/live-image-amd64.hybrid.iso.html ${PAGE}
			output_echo "Warning: diffoscope detected differences in the images."
		else
			export PAGE=${CONFIGURATION}-${DEBIAN_VERSION}.txt
			printenv >${PAGE}
			output_echo "Error: Something went wrong."
		fi
		if [ ! -z "${RESULTSDIR}" ]; then
			# Do not publish the ISO images as artifact, they would be able to consume too much disk space
			rm -rf ${RESULTSDIR}/b1
			rm -rf ${RESULTSDIR}/b2

			TMPDIR=${RESULTSDIR}
			save_artifacts debian_live_build ${CONFIGURATION}-${DEBIAN_VERSION} https://wiki.debian.org/ReproducibleInstalls/LiveImages
		fi
	fi
	publish_page debian_live_build

	output_echo Cleanup $1
	# Cleanup the workspace
	if [ ! -z "${BUILDDIR}" ]; then
		sudo rm -rf --one-file-system ${BUILDDIR}
	fi
	# Cleanup the results
	if [ ! -z "${RESULTSDIR}" ]; then
		rm -rf --one-file-system ${RESULTSDIR}
	fi
}

#
# main: follow https://wiki.debian.org/ReproducibleInstalls/LiveImages
#

# Argument 1 = image type
export CONFIGURATION="$1"

# Argument 2 = Debian version
export DEBIAN_VERSION="$2"

# Two arguments are required
if [ -z "${CONFIGURATION}" -o -z "${DEBIAN_VERSION}" ]; then
	output_echo "Error: Bad command line arguments."
	exit 1
fi

# Cleanup if something goes wrong
trap cleanup INT TERM EXIT

if $DEBUG; then
	export WGET_OPTIONS=
else
	export WGET_OPTIONS=--quiet
fi

# Randomize start time
delay_start

# Cleanup possible artifacts of a previous build (see reproducible_common.sh for the path)
rm -rf $BASE/debian_live_build/artifacts/r00t-me/${CONFIGURATION}-${DEBIAN_VERSION}_tmp-*

# Generate and use an isolated workspace
export PROJECTNAME="live-build"
mkdir -p /srv/workspace/live-build
export BUILDDIR=$(mktemp --tmpdir=/srv/workspace/live-build -d -t ${CONFIGURATION}-${DEBIAN_VERSION}.XXXXXXXX)
cd ${BUILDDIR}
export RESULTSDIR=$(mktemp --tmpdir=/srv/reproducible-results -d -t ${PROJECTNAME}-${CONFIGURATION}-${DEBIAN_VERSION}-XXXXXXXX) # accessible in schroots, used to compare results

# Fetch the rebuild script (and nothing else)
git clone https://salsa.debian.org/live-team/live-build.git rebuild_script --no-checkout --depth 1
cd rebuild_script
git checkout HEAD test/rebuild.sh
cd ..

# First build
output_echo "Running the rebuild script for the 1st build."
set +e # We are interested in the return value
# The 3rd argument is provided to allow for local testing of this script.
# - If SNAPSHOT_TIMESTAMP is already set, use that timestamp instead of the latest snapshot
rebuild_script/test/rebuild.sh "$1" "$2" "${SNAPSHOT_TIMESTAMP}"
RETURNVALUE=$?
grep --quiet --no-messages "Build result: 0" summary.txt
BUILD_OK_FOUND=$?

# Retrieve the timestamp that was used
export SNAPSHOT_TIMESTAMP=$(grep --no-messages "Snapshot timestamp:" summary.txt | cut -f 3 -d " ")

set -e

if [ ${RETURNVALUE} -ne 0 -o ${BUILD_OK_FOUND} -ne 0 ]; then
	# Something went wrong. Perhaps an alternative timestamp is proposed
	SNAPSHOT_TIMESTAMP=$(grep --no-messages "Alternative timestamp:" summary.txt | cut -f 3 -d " ")
	if [ -z ${SNAPSHOT_TIMESTAMP} ]; then
		output_echo "Error: the image could not be built, no alternative was proposed."
		exit 1
	fi
	output_echo "Warning: the build failed with ${RETURNVALUE}. The latest snapshot might not be complete (yet). Now trying again using the previous snapshot instead."
	rebuild_script/test/rebuild.sh "$1" "$2" "${SNAPSHOT_TIMESTAMP}"
fi

# Move the image away
mkdir -p ${RESULTSDIR}/b1/${PROJECTNAME}/${CONFIGURATION}
mv live-image-amd64.hybrid.iso ${RESULTSDIR}/b1/${PROJECTNAME}/${CONFIGURATION}
mv summary.txt ${RESULTSDIR}/summary_build1.txt

# Second build
output_echo "Running the rebuild script for the 2nd build."
rebuild_script/test/rebuild.sh "$1" "$2" "${SNAPSHOT_TIMESTAMP}"

# Move the image away
mkdir -p ${RESULTSDIR}/b2/${PROJECTNAME}/${CONFIGURATION}
mv live-image-amd64.hybrid.iso ${RESULTSDIR}/b2/${PROJECTNAME}/${CONFIGURATION}
mv summary.txt ${RESULTSDIR}/summary_build2.txt

# Clean up
output_echo "Running lb clean after the 2nd build."
sudo lb clean --purge

# The workspace is no longer required
cd ..

# Run diffoscope on the images
output_echo "Calling diffoscope on the results."
TIMEOUT="240m"
DIFFOSCOPE="$(schroot --directory /tmp -c chroot:jenkins-reproducible-${DBDSUITE}-diffoscope diffoscope -- --version 2>&1)"
TMPDIR=${RESULTSDIR}
call_diffoscope ${PROJECTNAME} ${CONFIGURATION}/live-image-amd64.hybrid.iso

cleanup success

# Turn off the trap
trap - INT TERM EXIT

# We reached the end, return with PASS
exit 0
