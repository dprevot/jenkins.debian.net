#!/bin/bash

set -e

docker run --rm --cap-add=SYS_ADMIN --privileged \
	--memory=20g \
	-v /srv/janitor:/srv/janitor \
	-e BUILD_URL="${BUILD_URL}" \
	-e NODE_NAME="${NODE_NAME}" \
	registry.salsa.debian.org/jelmer/janitor.debian.net/worker \
	--listen-address=0.0.0.0 \
	--port=8080 \
	--base-url=https://janitor.debian.net/api/ \
	--credentials=/srv/janitor/credentials.json \
	--tee
